use std::rc::Rc;
use std::cell::RefCell;
use petrinet::{Petrinet, Place, Transition, Orientation};

fn main() {
    let mut petrinet: Petrinet = Petrinet::new();
    
    let place_index_1: Rc<RefCell<Place>> = petrinet.add_place(String::from("P1"), 4);
    let place_index_2: Rc<RefCell<Place>> = petrinet.add_place(String::from("P2"), 0);
    let place_index_3: Rc<RefCell<Place>> = petrinet.add_place(String::from("P3"), 0);
    
    let transition_index_1: Rc<RefCell<Transition>> = petrinet.add_transition(String::from("T1"));
    let transition_index_2: Rc<RefCell<Transition>> = petrinet.add_transition(String::from("T2"));
    
    // P1 -> T1
    petrinet.add_edge(&place_index_1, &transition_index_1, Orientation::Transition, 1);
    // P2 -> T2
    petrinet.add_edge(&place_index_2, &transition_index_2, Orientation::Transition, 1);
    // P3 -> T2
    petrinet.add_edge(&place_index_3, &transition_index_2, Orientation::Transition, 1);
    // T1 -> P2
    petrinet.add_edge(&place_index_2, &transition_index_1, Orientation::Place, 1);
    // T1 -> P3
    petrinet.add_edge(&place_index_3, &transition_index_1, Orientation::Place, 1);
    // T2 -> P2
    petrinet.add_edge(&place_index_2, &transition_index_2, Orientation::Place, 1);

    petrinet.display_petrinet();

    println!("{fire_result}", fire_result = petrinet.fire(&transition_index_1));

    petrinet.display_petrinet();
}

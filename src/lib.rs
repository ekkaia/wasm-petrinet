use std::{cell::RefCell, rc::Rc};

#[derive(Debug, Clone, Copy)]
pub enum Orientation {
    Transition,
    Place,
}

#[derive(Debug, Clone)]
pub struct Edge {
    pub place: Rc<RefCell<Place>>,
    pub transition: Rc<RefCell<Transition>>,
    pub orientation: Orientation,
    pub weight: i32,
}

#[derive(Debug, Clone)]
pub struct Place {
    pub name: String,
    pub tokens: i32,
    pub input_edges: Vec<Rc<RefCell<Edge>>>,
    pub output_edges: Vec<Rc<RefCell<Edge>>>,
}

#[derive(Debug, Clone)]
pub struct Transition {
    pub name: String,
    pub input_edges: Vec<Rc<RefCell<Edge>>>,
    pub output_edges: Vec<Rc<RefCell<Edge>>>,
}

pub struct Petrinet {
    pub places: Vec<Rc<RefCell<Place>>>,
    pub transitions: Vec<Rc<RefCell<Transition>>>,
    pub edges: Vec<Rc<RefCell<Edge>>>,
}

impl Petrinet {
    pub fn new() -> Petrinet {
        return Petrinet {
            places: Vec::new(),
            transitions: Vec::new(),
            edges: Vec::new(),
        };
    }

    pub fn add_place(&mut self, name: String, tokens: i32) -> Rc<RefCell<Place>> {
        let place: Place = Place {
            name,
            tokens,
            input_edges: Vec::new(),
            output_edges: Vec::new(),
        };
        let refcell_place: RefCell<Place> = RefCell::new(place);
        let rc_place: Rc<RefCell<Place>> = Rc::new(refcell_place);
        let rc_clone_place: Rc<RefCell<Place>> = Rc::clone(&rc_place);
        self.places.push(rc_place);
        return rc_clone_place;
    }

    pub fn add_transition(&mut self, name: String) -> Rc<RefCell<Transition>> {
        let transition: Transition = Transition {
            name,
            input_edges: Vec::new(),
            output_edges: Vec::new(),
        };
        let refcell_transition: RefCell<Transition> = RefCell::new(transition);
        let rc_transition: Rc<RefCell<Transition>> = Rc::new(refcell_transition);
        let rc_clone_transition: Rc<RefCell<Transition>> = Rc::clone(&rc_transition);
        self.transitions.push(rc_transition);
        return rc_clone_transition;
    }

    pub fn add_edge(
        &mut self,
        rc_place: &Rc<RefCell<Place>>,
        rc_transition: &Rc<RefCell<Transition>>,
        orientation: Orientation,
        weight: i32,
    ) {
        let edge: Edge = Edge {
            place: Rc::clone(rc_place),
            transition: Rc::clone(rc_transition),
            orientation,
            weight,
        };
        let ref_edge: RefCell<Edge> = RefCell::new(edge);
        let rc_edge: Rc<RefCell<Edge>> = Rc::new(ref_edge);
        self.edges.push(Rc::clone(&rc_edge));
        match orientation.clone() {
            Orientation::Place => {
                rc_place.borrow_mut().input_edges.push(Rc::clone(&rc_edge));
                rc_transition.borrow_mut().output_edges.push(Rc::clone(&rc_edge));
            },
            Orientation::Transition => {
                rc_place.borrow_mut().output_edges.push(Rc::clone(&rc_edge));
                rc_transition.borrow_mut().input_edges.push(Rc::clone(&rc_edge));
            }
        }
    }

    pub fn display_petrinet(&self) {
        for edge in &self.edges {
            match edge.borrow().orientation {
                Orientation::Transition => {
                    println!("{place}[{toks}] -({w})-> {trans}",
                        trans = edge.borrow().transition.borrow().name,
                        place = edge.borrow().place.borrow().name,
                        w = edge.borrow().weight,
                        toks = edge.borrow().place.borrow().tokens
                    );
                },
                Orientation::Place => {
                    println!("{trans} -({w})-> {place}[{toks}]",
                        trans = edge.borrow().transition.borrow().name,
                        place = edge.borrow().place.borrow().name,
                        w = edge.borrow().weight,
                        toks = edge.borrow().place.borrow().tokens
                    );
                }
            }
        }
    }

    pub fn is_fire_possible(&self, rc_transition: &Rc<RefCell<Transition>>) -> bool {
        let trans = rc_transition.borrow();
        for edge in trans.input_edges.iter() {
            let place = &edge.borrow().place;
            if edge.borrow().weight > place.borrow().tokens {
                return false;
            }
        }
        return true;
    }

    pub fn fire(&mut self, rc_transition: &Rc<RefCell<Transition>>) -> bool {
        let is_fire_possible: bool = self.is_fire_possible(rc_transition);

        if !is_fire_possible {
            return false;
        }

        let trans = rc_transition.borrow();
        for edge in trans.input_edges.iter() {
            let place = &edge.borrow().place;
            place.borrow_mut().tokens -= edge.borrow().weight;
        }
        for edge in trans.output_edges.iter() {
            let place = &edge.borrow().place;
            place.borrow_mut().tokens += edge.borrow().weight;
        }
        return true;
    }
}

pub type EdgeIndex = usize;
pub type TransitionIndex = usize;
pub type PlaceIndex = usize;

#[derive(Debug, Clone, Copy)]
pub enum Orientation {
    Transition,
    Place,
}

pub struct Edge {
    pub place: PlaceIndex,
    pub transition: TransitionIndex,
    pub orientation: Orientation,
	pub weight: i32,
}

#[derive(Debug, Clone)]
pub struct Place {
    pub name: String,
    pub tokens: i32,
    pub input_edges: Vec<EdgeIndex>,
    pub output_edges: Vec<EdgeIndex>,
}

#[derive(Debug, Clone)]
pub struct Transition {
    pub name: String,
    pub input_edges: Vec<EdgeIndex>,
    pub output_edges: Vec<EdgeIndex>,
}

pub struct Petrinet {
    pub places: Vec<Place>,
    pub transitions: Vec<Transition>,
    pub edges: Vec<Edge>,
}

impl Petrinet {
    pub fn new() -> Petrinet {
        return Petrinet {
            places: Vec::new(),
            transitions: Vec::new(),
            edges: Vec::new(),
        };
    }

    pub fn add_place(&mut self, name: String, tokens: i32) -> PlaceIndex {
        let index: usize = self.places.len();
        self.places.push(Place { 
            name,
            tokens,
            input_edges: Vec::new(),
            output_edges: Vec::new(),
        });
        return index;
    }

    pub fn add_transition(&mut self, name: String) -> PlaceIndex {
        let index: usize = self.transitions.len();
        self.transitions.push(Transition { 
            name,
            input_edges: Vec::new(),
            output_edges: Vec::new(),
        });
        return index;
    }

    pub fn add_edge(&mut self, place: PlaceIndex, transition: TransitionIndex, orientation: Orientation, weight: i32) -> EdgeIndex {
        let index: usize = self.edges.len();
        self.edges.push(Edge { 
            place,
            transition,
            orientation,
            weight,
        });
        match orientation.clone() {
            Orientation::Place => {
                let _ = &self.places[place].input_edges.push(index);
                let _ = &self.transitions[transition].output_edges.push(index);
            },
            Orientation::Transition => {
                let _ = &self.places[place].output_edges.push(index);
                let _ = &self.transitions[transition].input_edges.push(index);
            }
        }
        return index;
    }

    pub fn display_petrinet(&self) {
        for edge in &self.edges {
            match edge.orientation {
                Orientation::Transition => {
                    println!("{place}[{toks}] -({w})-> {trans}",
                        trans = &self.transitions[edge.transition].name,
                        place = &self.places[edge.place].name,
                        w = edge.weight,
                        toks = &self.places[edge.place].tokens
                    );
                },
                Orientation::Place => {
                    println!("{trans} -({w})-> {place}[{toks}]",
                        trans = &self.transitions[edge.transition].name,
                        place = &self.places[edge.place].name,
                        w = edge.weight,
                        toks = &self.places[edge.place].tokens
                    );
                }
            }
        }
    }

    pub fn is_fire_possible(&self, trans_index: TransitionIndex) -> bool {
        for edge_index in &self.transitions[trans_index].input_edges {
            let place_index = &self.edges[*edge_index].place;
            if &self.edges[*edge_index].weight > &self.places[*place_index].tokens {
                return false;
            }
        }
        return true;
    }

    pub fn fire(&mut self, trans_index: TransitionIndex) -> String {
        let is_fire_possible: bool = self.is_fire_possible(trans_index);
        match is_fire_possible {
            true => {
                for edge_index in &self.transitions[trans_index].input_edges {
                    let place_index = &self.edges[*edge_index].place;
                    self.places[*place_index].tokens -= &self.edges[*edge_index].weight;
                }
                for edge_index in &self.transitions[trans_index].output_edges {
                    let place_index = &self.edges[*edge_index].place;
                    self.places[*place_index].tokens += &self.edges[*edge_index].weight;
                }
                return String::from("Firing executed successfully");
            },
            false => {
                return String::from("Impossible to fire, not enough tokens.");
            }
        }
    }
}

use text_io::scan;

pub struct Petrinet {
	pub w: Vec<Vec<i32>>,
	pub marking: Vec<i32>,
}

impl Petrinet {
	pub fn new(w_plus: Vec<Vec<i32>>, w_minus: Vec<Vec<i32>>, marking: Vec<i32>) -> Petrinet {
		let mut w: Vec<Vec<i32>> = Vec::new();
		for n in 0..w_plus.len() {
			w.push(Vec::new());
			for m in 0..w_plus[n].len() {
				w[n].push(w_plus[n][m] - w_minus[n][m]);
			}
		}
		return Petrinet {
			w,
			marking,
		};
	}

	pub fn fire(&mut self, transition_fire_count: Vec<i32>) -> Option<Vec<i32>> {
		let mut c: Vec<i32> = Vec::new();
		let mut marking_tmp: Vec<i32> = self.marking.clone();
		for n in 0..self.w.len() {
			let mut tmp: i32 = 0;
			for m in 0..self.w[n].len() {
				tmp += self.w[n][m] * transition_fire_count[m];
			}

			if marking_tmp[n] + tmp >= 0 {
				marking_tmp[n] += tmp;
				c.push(tmp);
			} else {
				return None;
			}
		}
		self.marking = marking_tmp;
		return Some(c);
	}
}

fn cli() {
    println!("Welcome to Petrinet CLI.");
    println!("How many places your petrinet has?");
    let places_count: usize;
    scan!("{}", places_count);
    println!("How many transitions your petrinet has?");
    let transitions_count: usize;
    scan!("{}", transitions_count);
    let mut w_plus: Vec<Vec<i32>> = Vec::new();
    let mut w_minus: Vec<Vec<i32>> =  Vec::new();

    for _ in 0..places_count {
        w_plus.push(vec![0; transitions_count]);
        w_minus.push(vec![0; transitions_count]);
    }

    for p in 0..places_count {
        let mut tmp: usize;
        println!("To which transitions is the place {place} connected? Enter the transition numbers or 0 if the place is not connected to any transition.", place = p + 1);
        scan!("{}", tmp);

        if tmp != 0 {
            w_minus[p][tmp - 1] = 1;
        }

        while tmp != 0 {
            println!("Enter the next transition number or 0 if the place is not connected to another transition.");
            scan!("{}", tmp);
            if tmp != 0 {
                w_minus[p][tmp - 1] = 1;
            }
        }
    }

    println!("Current place-to-transition matrix:");
    display_vector(w_minus.clone());

    for t in 0..transitions_count {
        let mut tmp: usize;
        println!("To which places is the transition {transition} connected? Enter the place numbers or 0 if the transition is not connected to any place.", transition = t + 1);
        scan!("{}", tmp);

        if tmp != 0 {
            w_plus[tmp - 1][t] = 1;
        }

        while tmp != 0 {
            println!("Enter the next place number or 0 if the transition is not connected to another place.");
            scan!("{}", tmp);
            if tmp != 0 {
                w_plus[tmp - 1][t] = 1;
            }
        }
    }

    println!("Current transition-to-place matrix:");
    display_vector(w_plus.clone());

    let mut marking: Vec<i32> = Vec::new();

    println!("Enter the number of token on which place:");
    for p in 0..places_count {
        println!("Place N°{place}", place = p + 1);
        let tmp: i32;
        scan!("{}", tmp);
        marking.push(tmp);
    }
    

    let mut petrinet: Petrinet = Petrinet::new(w_plus, w_minus, marking);
    println!("Current petrinet marking: {:?}", petrinet.marking);

    println!("Now you can fire. For each transition, enter how many times you want to fire:");
    let mut transition_fire_count: Vec<i32> = Vec::new();
    for t in 0..transitions_count {
        println!("Transition N°{transition}", transition = t + 1);
        let tmp: i32;
        scan!("{}", tmp);
        transition_fire_count.push(tmp);
    }

    let result: Option<Vec<i32>> = petrinet.fire(transition_fire_count);

    match result {
        Some(_) => {
            println!("{:?}", result.unwrap());
            println!("New petrinet marking: {:?}", petrinet.marking);
        },
        None => println!("Transition fire impossible! Not enough tokens in places"),
    }
}
